// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig:{
    apiKey: "AIzaSyCnIq8CbGLj2D_O6kSNkzMMrc3s_diF5vQ",
    authDomain: "idpets-af273.firebaseapp.com",
    projectId: "idpets-af273",
    storageBucket: "idpets-af273.appspot.com",
    messagingSenderId: "900033101442",
    appId: "1:900033101442:web:33e627210bc907b545be9b",
    measurementId: "G-813D3DX7X3"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
