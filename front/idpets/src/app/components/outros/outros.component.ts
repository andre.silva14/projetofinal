import { Component, OnInit } from '@angular/core';
import { Animal } from 'src/app/models/animal';
import { AnimalService } from 'src/app/services/animal.service';

@Component({
  selector: 'app-outros',
  templateUrl: './outros.component.html',
  styleUrls: ['./outros.component.css']
})
export class OutrosComponent implements OnInit {
  animais : Animal[] = {} as Animal[];


  constructor(private animalService: AnimalService) { }

  ngOnInit(): void {
    this.animalService.readOutros().subscribe(o =>{
      this.animais = o;
    })
  }

}
