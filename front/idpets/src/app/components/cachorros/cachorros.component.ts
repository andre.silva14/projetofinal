import { Component, OnInit } from '@angular/core';
import { Animal } from 'src/app/models/animal';
import { AnimalService } from 'src/app/services/animal.service';

@Component({
  selector: 'app-cachorros',
  templateUrl: './cachorros.component.html',
  styleUrls: ['./cachorros.component.css']
})
export class CachorrosComponent implements OnInit {
  animais : Animal[] = {} as Animal[];


  constructor(private animalService: AnimalService) { }

  ngOnInit(): void {
    this.animalService.readCachorros().subscribe(c =>{
      console.log(c);
      this.animais = c;
      
    })
  }

}
