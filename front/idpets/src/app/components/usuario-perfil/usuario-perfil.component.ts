import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { ActivatedRoute } from '@angular/router';
import { UsuarioService } from 'src/app/services/usuario.service';
import { Usuario } from 'src/app/models/usuario';
import { Endereco } from 'src/app/models/endereco';
import { ConsultaCepService } from 'src/app/services/consulta-cep.service';

@Component({
  selector: 'app-usuario-perfil',
  templateUrl: './usuario-perfil.component.html',
  styleUrls: ['./usuario-perfil.component.css']
})
export class UsuarioPerfilComponent implements OnInit {
  public usuario = {} as Usuario;
  public endereco = {} as Endereco;
  public cep = '';

  constructor(private auth : AuthService, private route: ActivatedRoute, private usuarioService: UsuarioService, private cepService:ConsultaCepService) { }

  ngOnInit(): void {
    this.auth.validaUsuario();

    this.route.params.subscribe(u=> {
      let id = u['id'];
      if( id != null){
        this.carregaUsuario(Number(id))
      }
    })
  }


  private carregaUsuario(id:number){
    this.usuarioService.readById(id).subscribe(u=> this.usuario = u);
  }

  salvar(){
    if(this.usuario.id != null){
      this.usuarioService.update(this.usuario).subscribe(f=>{});
    }
  }


  deslogar(){
    this.auth.logout();
  }

  consultaCEP(target:any){
    if(target instanceof EventTarget) {
      var elemento = target as HTMLInputElement;
      this.cep = elemento.value;
    }

    if(this.cep!=null && this.cep!==''){
      this.cepService.consultaCep(this.cep).subscribe(dados => this.populaDadosForm(dados));

    }


  }
  populaDadosForm(dados:any){
    this.endereco.cep = dados.cep;
    this.endereco.estado =dados.uf;
    this.endereco.cidade = dados.localidade;
    this.endereco.bairro=dados.bairro;
    this.endereco.logradouro=dados.logradouro;

    console.log(this.endereco)

  }
}
