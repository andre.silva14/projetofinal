import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidaEmailComponent } from './valida-email.component';

describe('ValidaEmailComponent', () => {
  let component: ValidaEmailComponent;
  let fixture: ComponentFixture<ValidaEmailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ValidaEmailComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ValidaEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
