import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Usuario } from 'src/app/models/usuario';
import { AuthService } from 'src/app/services/auth.service';
import { UsuarioService } from 'src/app/services/usuario.service';
import { ConsultaCepService } from 'src/app/services/consulta-cep.service';
import { EnderecoService } from 'src/app/services/endereco.service';
import { Endereco } from 'src/app/models/endereco';
import { getToken } from 'firebase/app-check';



@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {
  public usuario = {} as Usuario;
  public endereco = {} as Endereco;

  public senhaConfirmacao = {};
  public cep = '';


  constructor(private route:ActivatedRoute, private usuarioService:UsuarioService, private auth : AuthService, private cepService:ConsultaCepService, private enderecoService: EnderecoService) { }


   ngOnInit(): void {
    this.route.params.subscribe(u => {
      let id = u['id'];
      if (id != null) {
        this.carregarUsuario(Number(id));
      }
    });
  }

  cadastrar() {
    this.enderecoService.creat(this.endereco).subscribe(e => {
      console.log(e);
    });
    
    this.usuario.endereco = this.endereco;
    this.usuarioService.create(this.usuario).subscribe(r => {
      console.log(r);
     });
    }

  private carregarUsuario(id:number) {
    this.usuarioService.readById(id).subscribe(u => this.usuario = u);
  }

  registrar() {
    if(this.usuario.email == '') {
      alert('Please enter email');
      return;
    }
    if(this.usuario.senha == '') {
      alert('Please enter senha');
      return;
    }
    this.auth.registrar(this.usuario.email,this.usuario.senha);
    
    this.usuario.email = '';
    this.usuario.senha = '';
  }

  cadastre(){
    console.log(this.usuario.senha)
    console.log(this.senhaConfirmacao)
    if (this.usuario.senha == this.senhaConfirmacao) {
    this.cadastrar();
    this.registrar();
    this.saveEndereco();
  } else{
    alert("As senhas não coincidem");
  }
  }

  consultaCEP(target:any){
    if(target instanceof EventTarget) {
      var elemento = target as HTMLInputElement;
      this.cep = elemento.value;
    }

    if(this.cep!=null && this.cep!==''){
      this.cepService.consultaCep(this.cep).subscribe(dados => this.populaDadosForm(dados));

    }
  }

  saveEndereco(){
    this.enderecoService.creat(this.endereco);
  }

 
  populaDadosForm(dados:any){
    this.endereco.cep = dados.cep;
    this.endereco.estado =dados.uf;
    this.endereco.cidade = dados.localidade;
    this.endereco.bairro=dados.bairro;
    this.endereco.logradouro=dados.logradouro;

    console.log(this.endereco)

  }

 
  

}
