import { Component, OnInit } from '@angular/core';
import { Animal } from 'src/app/models/animal';
import { AnimalService } from 'src/app/services/animal.service';

@Component({
  selector: 'app-gatos',
  templateUrl: './gatos.component.html',
  styleUrls: ['./gatos.component.css']
})
export class GatosComponent implements OnInit {
  animais : Animal[] = {} as Animal[];

  constructor(private animalService: AnimalService) { }

  ngOnInit(): void {
      this.animalService.readGatos().subscribe(g =>{
        this.animais = g;
      })
    }
  }

