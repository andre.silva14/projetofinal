import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email : string = '';
  senha : string = ''; 

  constructor(private auth : AuthService) { }

  ngOnInit(): void {
  }

  login(){
    if(this.email == ''){
      alert('Insira um email válido');
      return;
    }

    if(this.senha == ''){
      alert('Senha inválida');
      return;
    }

    this.auth.login(this.email, this.senha);

    this.email = '';
    this.senha = '';
  }

  signInComGoogle(){
    this.auth.googleSignIn();
  }

  signInComFacebook(){
    this.auth.FacebookSignIn();
  }

}
