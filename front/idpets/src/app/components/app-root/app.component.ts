import { MenuItem } from "primeng/api";
import { Component } from '@angular/core';
import { AuthService } from "src/app/services/auth.service";
import { AnimalComponent } from "../animalCadastrar/animal.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
   title = 'IDpets'
   items!: MenuItem[];
   
 
  ngOnInit() {
    this.items = [
        {
           label:'Página inicial',
           icon:'pi pi-fw pi-home',
           routerLink:'/home'
        },
        {
           label:'Pets',
           icon:'pi pi-fw pi-heart',
           items:[
              {
                 label:'Cachorros',
                 icon:'pi pi-fw ',
                 routerLink:'/animal/cachorro'

              },
              {
                 label:'Gatos',
                 icon:'pi pi-fw ',
                 routerLink:'/animal/gato'
              },
              {
                 label:'Outros',
                 icon:'pi pi-fw ',
                 routerLink:'/animal/outro',
                  
              }
           ]
        },
        {
           separator:true
        },
        {
           label: 'Usuario',
           icon:'pi pi-fw pi-user',
           routerLink:'/perfil'
        }
    ];
}




}
