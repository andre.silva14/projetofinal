import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SelectItem } from 'primeng/api';
import { Animal } from 'src/app/models/animal';
import { AuthService } from 'src/app/services/auth.service';
import { Foto } from 'src/app/models/foto';
import { AnimalService } from 'src/app/services/animal.service';
import { FotoService } from 'src/app/services/foto.service';

@Component({
  selector: 'app-animal',
  templateUrl: './animal.component.html',
  styleUrls: ['./animal.component.css']
})
export class AnimalComponent implements OnInit {
  animal: Animal = {} as Animal;
  foto: Foto = {} as Foto;

  especies: SelectItem[];
  porte: SelectItem[];
  sexo: SelectItem[];
  selectedEspecie: SelectItem;
  selectedPorte: SelectItem;
  selectedSexo: SelectItem;

  imagens: any[] = [];

  constructor(private route: ActivatedRoute,private fotoService: FotoService, private animalService: AnimalService, private auth : AuthService) {
    this.especies = [
      { label: 'Cachorro', value: 'CACHORRO'},
      { label: 'Gato', value: 'GATO' },
      { label: 'Outro',value: 'OUTRO' }
    ];

    this.selectedEspecie = { label: 'Selecione', value: '' };

    this.porte = [
      { label: 'Grande', value: 'GRANDE' },
      { label: 'Médio', value: 'MEDIO' },
      { label: 'Pequeno', value: 'PEQUENO' }
    ];

    this.selectedPorte = { label: 'Selecione', value: '' };

    this.sexo = [
      { label: 'Macho', value: 'MACHO' },
      { label: 'Femea', value: 'FEMEA' }
    ];

    this.selectedSexo = { label: 'Selecione', value: '' };

  }

  ngOnInit(): void {
    this.auth.validaUsuario();
    this.route.params.subscribe( animal => {
      let id = animal['id'];
      if (id != null) {
        this.carregarAnimal(Number(id));
      }
    });
  }

  salvar() {
    if (this.animal.id == null) {
      this.animal.especie = this.selectedEspecie;
      this.animal.sexo = this.selectedSexo;
      this.animal.porte = this.selectedPorte;
      this.foto.animal = this.animal;
      this.animalService.create(this.animal).subscribe(resp => {
        console.log(resp);
        if (this.foto.id == null) {
          this.fotoService.create(this.foto).subscribe(res => {
            console.log(res);
          });
        }
      });
      console.log(this.foto.foto)
    } else {
      this.animalService.update(this.animal).subscribe(resp => {
        console.log(resp);
      });
    }
  }

  //falta linkar imagem com o animal, nos precisamos de ajuda nisso
  myUploader(event: any) {
    let fileReader = new FileReader();
    for (let file of event.files) {
      fileReader.readAsDataURL(file);
      fileReader.onload = this.carregaFoto
     
    } 
    }

carregaFoto(file: any) {
  this.foto = file.target.result;
  console.log(this.foto, this.foto)
}


  private carregarAnimal(id: number) {
    this.animalService.readById(id).subscribe(animal => this.animal = animal);
  }
}