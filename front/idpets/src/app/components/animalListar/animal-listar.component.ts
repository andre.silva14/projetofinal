import { Component, OnInit } from '@angular/core';
import { Animal } from 'src/app/models/animal';
import { AnimalService } from 'src/app/services/animal.service';

@Component({
  selector: 'app-animal-listar',
  templateUrl: './animal-listar.component.html',
  styleUrls: ['./animal-listar.component.css']
})
export class AnimalListarComponent implements OnInit {
  animais : Animal[] = {} as Animal[];

  constructor(private animalService: AnimalService) {

  }

  ngOnInit(): void {
    this.animalService.readAll().subscribe(animais => {
			this.animais = animais;
		});
  }

  

}
