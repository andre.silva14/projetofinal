import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import {CheckboxModule} from 'primeng/checkbox';
import { RadioButtonModule} from 'primeng/radiobutton'
import { FormsModule } from '@angular/forms';
import { TieredMenuModule } from 'primeng/tieredmenu';
import { ButtonModule } from 'primeng/button';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AccordionModule } from 'primeng/accordion';
import { AppComponent } from './components/app-root/app.component';
import { AnimalComponent } from './components/animalCadastrar/animal.component';
import { UsuarioComponent } from './components/usuarioCadastrar/usuario.component';
import { AnimalListarComponent } from './components/animalListar/animal-listar.component';
import {InputMaskModule} from 'primeng/inputmask';
import { InputTextareaModule} from 'primeng/inputtextarea';
import { PasswordModule } from 'primeng/password';
import { InputTextModule } from 'primeng/inputtext';
import { FIREBASE_OPTIONS } from '@angular/fire/compat';
import { environment } from 'src/environments/environment';
import {CarouselModule} from 'primeng/carousel';


import {FileUploadModule} from 'primeng/fileupload';
import { LoginComponent } from './components/login/login.component';
import { UsuarioPerfilComponent } from './components/usuario-perfil/usuario-perfil.component';
import { EsquecisenhaComponent } from './components/esquecisenha/esquecisenha.component';
import { VerificaEmailComponent } from './components/verifica-email/verifica-email.component';
import {DropdownModule} from 'primeng/dropdown';
import {TableModule} from 'primeng/table';
import { ValidaEmailComponent } from './components/valida-email/valida-email.component';
import { GatosComponent } from './components/gatos/gatos.component';
import { CachorrosComponent } from './components/cachorros/cachorros.component';
import { OutrosComponent } from './components/outros/outros.component';


@NgModule({
  declarations: [
    AppComponent,
    AnimalComponent,
    UsuarioComponent,
    AnimalListarComponent,
    LoginComponent,
    UsuarioPerfilComponent,
    EsquecisenhaComponent,
    ValidaEmailComponent,
    GatosComponent,
    CachorrosComponent,
    OutrosComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule, 
    AccordionModule, 
    FormsModule, 
    CheckboxModule, 
    RadioButtonModule, 
    AppRoutingModule,
    BrowserAnimationsModule,
    TieredMenuModule,
    ButtonModule,
    InputMaskModule,
    InputTextareaModule,
    PasswordModule,
    InputTextModule, 
    CarouselModule,
    FileUploadModule,
    DropdownModule,
    TableModule
  ],
  providers: [{ provide: FIREBASE_OPTIONS, useValue: environment.firebaseConfig }],
  bootstrap: [AppComponent],
})
export class AppModule {}

