import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConsultaCepService {

  constructor(private http:HttpClient) { }


  consultaCep(cep:string){
      return this.http.get(`http://viacep.com.br/ws/${cep}/json/`);
    
  }
}   

