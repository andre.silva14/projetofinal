import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Endereco } from '../models/endereco';

@Injectable({
  providedIn: 'root'
})
export class EnderecoService {
  private address = 'http://localhost:8080/endereco';

  constructor(private http:HttpClient) { }

  creat(endereco:Endereco):Observable<string> {
    return this.http.post(`${this.address}`, endereco, {responseType:'text'});
  }

  readById(id:number):Observable<Endereco> {
    return this.http.get<Endereco>(`${this.address}/${id}`)
  }

  update(endereco:Endereco):Observable<string> {
    return this.http.put(`${this.address}/${endereco.id}`, endereco, {responseType:'text'});
  }

  delete(id:number):Observable<string> {
    return this.http.delete(`${this.address}/${id}`, {responseType:'text'})
  }


}
