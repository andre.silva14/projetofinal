import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Usuario } from '../models/usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  private address = 'http://localhost:8080/usuario';

  constructor(private http:HttpClient) { }

  create(usuario:Usuario):Observable<string> {
    return this.http.post(`${this.address}`, usuario, {responseType:'text'});
  }

  readById(id:number):Observable<Usuario> {
    return this.http.get<Usuario>(`${this.address}/${id}`)
  }

  update(usuario:Usuario):Observable<string> {
    return this.http.put(`${this.address}/${usuario.id}`, usuario, {responseType:'text'});
  }

  delete(id:number):Observable<string> {
    return this.http.delete(`${this.address}/${id}`, {responseType:'text'})
  }

}
