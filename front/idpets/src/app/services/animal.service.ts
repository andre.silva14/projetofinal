import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Animal } from '../models/animal';

@Injectable({
  providedIn: 'root'
})
export class AnimalService {
  private address = 'http://localhost:8080/animal';

  constructor(private http:HttpClient) { }

  create(animal:Animal):Observable<string> {
    return this.http.post(`${this.address}`, animal, {responseType:'text'});
  }

  readAll():Observable<Animal[]> {
    return this.http.get<Animal[]>(`${this.address}`);
  }

  readById(id:number):Observable<Animal> {
    return this.http.get<Animal>(`${this.address}/${id}`);
  }

  readGatos():Observable<Animal[]> {
    return this.http.get<Animal[]>(`${this.address}/especie/GATO`);
  }

  readCachorros():Observable<Animal[]> {
    return this.http.get<Animal[]>(`${this.address}/especie/CACHORRO`);
  }
  readOutros():Observable<Animal[]> {
    return this.http.get<Animal[]>(`${this.address}/especie/OUTRO`);
  }
  update(animal:Animal):Observable<string> {
    return this.http.put(`${this.address}/${animal.id}`, animal, {responseType:'text'});
  }

  delete(id:number):Observable<string> {
    return this.http.delete(`${this.address}/${id}`, {responseType:'text'})
  }

}
