import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Foto } from '../models/foto';

@Injectable({
  providedIn: 'root'
})
export class FotoService {
  private address = 'http://localhost:8080/foto';

  constructor(private http:HttpClient) { }

  create(foto:Foto):Observable<string> {
    return this.http.post(`${this.address}`, foto, {responseType:'text'});
  }

  readAll():Observable<Foto[]> {
    return this.http.get<Foto[]>(`${this.address}`);
  }

  readById(id:number):Observable<Foto> {
    return this.http.get<Foto>(`${this.address}/${id}`);
  }

  update(foto:Foto):Observable<string> {
    return this.http.put(`${this.address}/${foto.id}`, foto, {responseType:'text'});
  }

  delete(id:number):Observable<string> {
    return this.http.delete(`${this.address}/${id}`, {responseType:'text'})
  }

}
