import { Endereco } from "./endereco"

export interface Usuario {
    id: number,
    nomeCompleto: string,
    endereco: Endereco,
    email: string,
    telefone: string,
    whatsapp: string,
    senha: string,
    experiencia:string
}