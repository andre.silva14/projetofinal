import { Usuario } from "./usuario"

export interface Animal {
    id: number,
    nome: string,
    sexo: any,
    especie: any,
    idade: string,
    porte: any,
    descricao: string,
    dono: Usuario

}
