import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AnimalListarComponent } from './components/animalListar/animal-listar.component';
import { AnimalComponent } from './components/animalCadastrar/animal.component';
import { UsuarioComponent } from './components/usuarioCadastrar/usuario.component';
import { LoginComponent } from './components/login/login.component';
import { UsuarioPerfilComponent } from './components/usuario-perfil/usuario-perfil.component';
import { EsquecisenhaComponent } from './components/esquecisenha/esquecisenha.component';
import { VerificaEmailComponent } from './components/verifica-email/verifica-email.component';
import { ValidaEmailComponent } from './components/valida-email/valida-email.component';
import { Component } from '@angular/core';
import { CachorrosComponent } from './components/cachorros/cachorros.component';
import { GatosComponent } from './components/gatos/gatos.component';
import { OutrosComponent } from './components/outros/outros.component';

const routes: Routes = [
  { path: 'usuario/cadastrar', component: UsuarioComponent }, 
  { path: 'animal/cadastrar', component: AnimalComponent },
  { path: 'login', component: LoginComponent},
  { path: 'perfil', component: UsuarioPerfilComponent},
  { path: 'home', component: AnimalListarComponent },
  { path: '', redirectTo: '/', pathMatch: 'full' },
  { path: 'login', component: LoginComponent},
  { path: 'recuperarsenha', component: EsquecisenhaComponent},
  { path: 'verifiqueseuemail', component: VerificaEmailComponent},
  { path: 'validacao', component: ValidaEmailComponent},
  { path: 'animal/cachorro', component: CachorrosComponent},
  { path: 'animal/gato', component: GatosComponent},
  { path: 'animal/outro', component: OutrosComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
