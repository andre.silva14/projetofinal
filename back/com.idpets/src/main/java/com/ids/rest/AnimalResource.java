package com.ids.rest;

import com.ids.dto.AnimalCreateRequest;
import com.ids.dto.ResponseError;
import com.ids.enumerator.Especie;
import com.ids.enumerator.Sexo;
import com.ids.model.Animal;
import com.ids.repository.AnimalRepository;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Set;

@Path("/animal")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AnimalResource {

    private Validator validator;
    private AnimalRepository repository;

    @Inject
    public AnimalResource(Validator validator, AnimalRepository repository) {
        this.validator = validator;
        this.repository = repository;
    }

    @POST
    @Transactional
    public Response creatAnimal(AnimalCreateRequest animal) {

        Set<ConstraintViolation<Object>> violations = validator.validate(animal);
        if (!violations.isEmpty()) {
            return ResponseError.createFromValidator(violations).returnWithStatusCode(422);
        }
        Animal animalEntity = new Animal();
        animalEntity.requestAnimal(animal);

        repository.persist(animalEntity);

        return Response.status(Response.Status.CREATED.getStatusCode()).entity(animalEntity).build();
    }

    @GET
    public Response listAllAnimal() {
        List<Animal> lista = repository.listAll();
        return Response.ok(lista).build();
    }

    @GET
    @Path("{id}")
    public Response listById(@PathParam("id") long id) {
        Animal model = repository.findById(id);
        if (model != null) {
            return Response.ok(model).build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }



    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/especie/{especie}")
    public Response animaisByEspecie(@PathParam("especie") String especie) {
        if (especie != null) {
            return Response.ok(Animal.findByEspecie(especie)).build();
        }
        return Response.ok(especie).build() ;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Animal> animaisBySexo(@QueryParam("sexo") String sexo) {
        if (sexo != null) {
            return Animal.findBySexo(sexo);
        }
        return Animal.listAll();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Animal> animaisByPorte(@QueryParam("porte") String porte) {
        if (porte != null) {
            return Animal.findByPorte(porte);
        }
        return Animal.listAll();
    }

    @PUT
    @Transactional
    @Path("{id}")
    public Response updateAnimal(@PathParam("id") long id, AnimalCreateRequest animal) {
        Set<ConstraintViolation<Object>> violations = validator.validate(animal);
        if (!violations.isEmpty()) {
            return ResponseError.createFromValidator(violations).returnWithStatusCode(422);
        }
        Animal animalEntity = repository.findById(id);
        if (animalEntity != null) {
            animalEntity.requestAnimal(animal);
            return Response.noContent().build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @DELETE
    @Transactional
    @Path("{id}")
    public Response deleteAnimal(@PathParam("id") long id) {
        Animal animalEntity = repository.findById(id);
        if (animalEntity != null) {
            repository.delete(animalEntity);
            return Response.noContent().build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }
}
