package com.ids.rest;

import com.ids.dto.EnderecoCreateRequest;
import com.ids.dto.ResponseError;
import com.ids.model.Endereco;
import com.ids.repository.EnderecoRepository;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Set;

@Path("/endereco")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class EnderecoResource {

    private EnderecoRepository enderecoRepository;

    private Validator validator;

    @Inject
    public EnderecoResource(EnderecoRepository enderecoRepository, Validator validator) {
        this.enderecoRepository = enderecoRepository;
        this.validator = validator;
    }

    @POST
    @Transactional
    public Response createEndereco(EnderecoCreateRequest endereco){
        Set<ConstraintViolation<Object>> violations = validator.validate(endereco);
        if(!violations.isEmpty()){
            return ResponseError.createFromValidator(violations).returnWithStatusCode(422);
        }
        Endereco enderecoEntity = new Endereco();
        enderecoEntity.requestEndereco(endereco);
        enderecoRepository.persist(enderecoEntity);

        return Response.status(Response.Status.CREATED.getStatusCode()).entity(enderecoEntity).build();
    }

    @GET
    @Path("{id}")
    public Response readById(@PathParam("id")long id){
        Endereco enderecoEntity = enderecoRepository.findById(id);
        if(enderecoEntity!= null){
            return Response.ok(enderecoEntity).build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @PUT
    @Transactional
    @Path("{id}")
    public Response upadateEndereco(@PathParam("id") long id, EnderecoCreateRequest endereco){
        Set<ConstraintViolation<Object>> violations = validator.validate(endereco);
        if(!violations.isEmpty()){
            return ResponseError.createFromValidator(violations).returnWithStatusCode(422);
        }
        Endereco enderecoEntity = enderecoRepository.findById(id);
        if(enderecoEntity != null) {
            enderecoEntity.requestEndereco(endereco);
            return Response.noContent().build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @DELETE
    @Transactional
    @Path("{id}")
    public Response deleteEndereco(@PathParam("id") long id) {
        Endereco enderecoEntity = enderecoRepository.findById(id);
        if (enderecoEntity != null) {
            enderecoRepository.delete(enderecoEntity);
            return Response.noContent().build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

}
