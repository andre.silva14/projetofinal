package com.ids.rest;

import com.ids.dto.ResponseError;
import com.ids.dto.UsuarioCreateRequest;
import com.ids.model.Animal;
import com.ids.model.Usuario;
import com.ids.repository.UsuarioRepository;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Set;
import java.util.List;

@Path("/usuario")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UsuarioResource{

    private UsuarioRepository usuarioRepository;

    private Validator validator;

    @Inject
    public UsuarioResource(UsuarioRepository usuarioRepository, Validator validator){
        this.usuarioRepository=usuarioRepository;
        this.validator=validator;
    }

    @POST
    @Transactional
    public Response createUsuario(UsuarioCreateRequest usuarioRequest){
        Set<ConstraintViolation<Object>> violations = validator.validate(usuarioRequest);
        if(!violations.isEmpty()){
            return ResponseError.createFromValidator(violations).returnWithStatusCode(422);
        }
        Usuario usuarioEntity = new Usuario();
        usuarioEntity.requestUsuario(usuarioRequest);

        usuarioRepository.persist(usuarioEntity);

        return Response.status(Response.Status.CREATED.getStatusCode()).entity(usuarioEntity).build();
    }

    @GET
    @Path("{id}")
    public Response readById(@PathParam("id")long id){
        Usuario model = usuarioRepository.findById(id);
        if(model!= null){
            return Response.ok(model).build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @GET
    public Response readAll(){
        List<Usuario> model = usuarioRepository.listAll();
        if(model!= null){
            return Response.ok(model).build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @PUT
    @Transactional
    @Path("{id}")
    public Response updateUsuario(@PathParam("id") long id, UsuarioCreateRequest usuario){
        Set<ConstraintViolation<Object>> violations = validator.validate(usuario);
        if(!violations.isEmpty()){
            return ResponseError.createFromValidator(violations).returnWithStatusCode(422);
        }
        Usuario model = usuarioRepository.findById(id);
        if(model != null) {
            model.requestUsuario(usuario);
            return Response.noContent().build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @DELETE
    @Transactional
    @Path("{id}")
    public Response deleteUsuario(@PathParam("id") long id) {
        Usuario model = usuarioRepository.findById(id);
        if (model != null) {
            usuarioRepository.delete(model);
            return Response.noContent().build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }
}
