package com.ids.rest;

import com.ids.dto.FotoCreateRequest;
import com.ids.dto.ResponseError;
import com.ids.model.Foto;
import com.ids.repository.FotoRepository;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Set;

@Path("/foto")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class FotoResource {

    private FotoRepository fotoRepository;

    private Validator validator;

    @Inject
    public FotoResource(FotoRepository fotoRepository, Validator validator) {
        this.fotoRepository = fotoRepository;
        this.validator = validator;
    }

    @POST
    @Transactional
    public Response createFoto(FotoCreateRequest foto){
        Set<ConstraintViolation<Object>> violations = validator.validate(foto);
        if(!violations.isEmpty()){
            return ResponseError.createFromValidator(violations).returnWithStatusCode(422);
        }
        Foto fotoEntity = new Foto();
        fotoEntity.requestFoto(foto);
        fotoRepository.persist(fotoEntity);

        return Response.status(Response.Status.CREATED.getStatusCode()).entity(fotoEntity).build();
    }

    @GET
    @Path("{id}")
    public Response readById(@PathParam("id")long id){
        Foto fotoEntity = fotoRepository.findById(id);
        if(fotoEntity!= null){
            return Response.ok(fotoEntity).build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @PUT
    @Transactional
    @Path("{id}")
    public Response updateFoto(@PathParam("id") long id, FotoCreateRequest foto){
        Set<ConstraintViolation<Object>> violations = validator.validate(foto);
        if(!violations.isEmpty()){
            return ResponseError.createFromValidator(violations).returnWithStatusCode(422);
        }
        Foto fotoEntity = fotoRepository.findById(id);
        if(fotoEntity != null) {
            fotoEntity.requestFoto(foto);
            return Response.noContent().build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @DELETE
    @Transactional
    @Path("{id}")
    public Response deleteFoto(@PathParam("id") long id) {
        Foto fotoEntity = fotoRepository.findById(id);
        if (fotoEntity != null) {
            fotoRepository.delete(fotoEntity);
            return Response.noContent().build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

}
