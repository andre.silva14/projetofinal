package com.ids.repository;

import com.ids.model.Animal;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import org.jboss.resteasy.annotations.Query;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;

@ApplicationScoped
public class AnimalRepository implements PanacheRepository<Animal> {

}
