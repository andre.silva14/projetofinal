package com.ids.repository;

import com.ids.model.Foto;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class FotoRepository implements PanacheRepository<Foto> {
}
