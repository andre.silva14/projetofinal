package com.ids.model;

import com.ids.dto.AnimalCreateRequest;
import com.ids.enumerator.Especie;
import com.ids.enumerator.Porte;
import com.ids.enumerator.Sexo;
import com.ids.repository.UsuarioRepository;
import io.quarkus.hibernate.orm.panache.PanacheEntity;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "animal")
public class Animal extends PanacheEntity {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nome;
    @Enumerated(EnumType.STRING)
    private Sexo sexo;
    @Enumerated(EnumType.STRING)
    private Especie especie;
    @Enumerated(EnumType.STRING)
    private Porte porte;
    private String idade;
    private String descricao;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_dono")
    private Usuario dono;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_foto")
    private Foto foto;

    public void requestAnimal(AnimalCreateRequest animalRequest) {
        this.nome = animalRequest.getNome();
        this.sexo = animalRequest.getSexo();
        this.especie = animalRequest.getEspecie();
        this.porte = animalRequest.getPorte();
        this.idade = animalRequest.getIdade();
        this.descricao = animalRequest.getDescricao();

        Usuario usuario = new Usuario();
        usuario = new UsuarioRepository().findById(animalRequest.getDonoId());
        this.dono = usuario;

        Foto foto = new Foto();
        foto.setFoto(animalRequest.getFoto());
        this.foto = foto;
    }

    public static List<Animal> findByEspecie(String especie) {
        return find("especie", Especie.valueOf(especie)).list();
    }

    public static List<Animal> findBySexo(String sexo) {
        return find("sexo", Especie.valueOf(sexo)).list();
    }

    public static List<Animal> findByPorte(String porte) {
        return find("porte", Especie.valueOf(porte)).list();
    }
}
