package com.ids.model;


import com.ids.dto.UsuarioCreateRequest;
import com.ids.enumerator.Especie;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.List;

@Data
@Entity
@Table(name = "usuario")
public class Usuario extends Pessoa {

    private String email;
    private String senha;
    private String telefone;
    private String whatsapp;
    private String experiencia;

    public void requestUsuario(UsuarioCreateRequest usuarioRequest) {
        this.nomeCompleto = usuarioRequest.getNomeCompleto();
        if (this.endereco == null && usuarioRequest.getEndereco() != null) {
            this.endereco = new Endereco();
        }
        this.endereco.requestEndereco(usuarioRequest.getEndereco());
        this.email = usuarioRequest.getEmail();
        this.senha = usuarioRequest.getSenha();
        this.telefone = usuarioRequest.getTelefone();
        this.whatsapp = usuarioRequest.getWhatsapp();
        this.experiencia = usuarioRequest.getExperiencia();
    }

}
