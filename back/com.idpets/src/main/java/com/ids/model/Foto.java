package com.ids.model;

import com.ids.dto.FotoCreateRequest;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "foto")
public class Foto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(columnDefinition="TEXT")
    private String foto;

    public void requestFoto(FotoCreateRequest fotoRequest) {
        this.foto = fotoRequest.getFoto();
    }

}


