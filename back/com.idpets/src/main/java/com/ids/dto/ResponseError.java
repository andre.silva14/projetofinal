package com.ids.dto;

import lombok.Data;

import javax.validation.ConstraintViolation;
import javax.ws.rs.core.Response;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Data
public class ResponseError {
    private String message;
    private Collection<FieldError> errors;

    public ResponseError(String message, Collection<FieldError> errors) {
        this.message = message;
        this.errors = errors;
    }

    public static ResponseError createFromValidator(Set<ConstraintViolation<Object>> violations ){
        Collection<FieldError> errors = violations.stream().map(error -> new FieldError(error.getMessage(), error.getPropertyPath().toString())).collect(Collectors.toList());
        String message = "Erro de validação de campos";
        return new ResponseError(message, errors);
    }

    public Response returnWithStatusCode(int code){
        return Response.status(422).entity(this).build();
    }
}
