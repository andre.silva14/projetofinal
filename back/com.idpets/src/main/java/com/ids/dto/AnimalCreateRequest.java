package com.ids.dto;

import com.ids.enumerator.Especie;
import com.ids.enumerator.Porte;
import com.ids.enumerator.Sexo;
import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class AnimalCreateRequest {

    @NotBlank(message = "Campo obrigatório")
    private String nome;
    @NotNull(message = "Campo obrigatório")
    private Sexo sexo;
    @NotNull(message = "Campo obrigatório")
    private Especie especie;
    @NotBlank(message = "Campo obrigatório")
    private String idade;
    @NotNull(message = "Campo obrigatório")
    private Porte porte;
    @NotBlank(message = "Campo obrigatório")
    private String descricao;
    private String foto;
    @DecimalMin(value = "0", inclusive = false, message = "Id dono não pode ser menor que 1")
    private long donoId;



}
