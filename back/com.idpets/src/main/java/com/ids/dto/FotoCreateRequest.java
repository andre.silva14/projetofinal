package com.ids.dto;

import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class FotoCreateRequest {


    private AnimalCreateRequest animal;
    private String foto;


}
