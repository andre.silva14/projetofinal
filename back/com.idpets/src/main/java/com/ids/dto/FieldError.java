package com.ids.dto;

import lombok.Data;

@Data
public class FieldError {
    private String message;
    private String field;

    public FieldError(String message, String field) {
        this.message = message;
        this.field = field;
    }
}
