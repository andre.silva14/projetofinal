package com.ids.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class UsuarioCreateRequest {

    @NotBlank(message = "Campo obrigatorio")
    private String nomeCompleto;
    private EnderecoCreateRequest endereco;
    @NotBlank(message = "Campo obrigatorio")
    private String email;
    @NotBlank(message = "Campo obrigatorio")
    private String senha;
    @NotBlank(message = "Campo obrigatorio")
    private String telefone;
    @NotBlank(message = "Campo obrigatorio")
    private String whatsapp;
    @NotBlank(message = "Campo obrigatorio")
    private String experiencia;

}
