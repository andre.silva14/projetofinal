package com.ids.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class EnderecoCreateRequest {

    @NotBlank(message = "Campo obrigatório")
    private String cep;
    @NotBlank(message = "Campo obrigatório")
    private String estado;
    @NotBlank(message = "Campo obrigatório")
    private String cidade;
    @NotBlank(message = "Campo obrigatório")
    private String bairro;
    @NotBlank(message = "Campo obrigatório")
    private String logradouro;
    @NotBlank(message = "Campo obrigatório")
    private String numero;

}
