package com.ids.rest;

import com.ids.dto.UsuarioCreateRequest;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@QuarkusTest
public class UsuarioResoucerTest {

    @Test
    public void createUsuario(){
        UsuarioCreateRequest usuario = new UsuarioCreateRequest();
        usuario.setNomeCompleto("");
        usuario.setEmail("");
        usuario.setTelefone("");
        usuario.setWhatsapp("");
        usuario.setExperiencia("");


        Response response = given().contentType(ContentType.JSON)
                .body(usuario)
                .when()
                .post("/usuario")
                .then().extract().response();
        assertEquals(javax.ws.rs.core.Response.Status.CREATED.getStatusCode(), response.getStatusCode());
        assertNotNull(response.jsonPath().getString("telefone"));
    }

}
